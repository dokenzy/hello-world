Django==2.2.9
psycopg2-binary==2.8.4

# REST
djangorestframework
django-filter
djangorestframework-jwt

# Test
flake8
pytest
pytest-cov
pytest-django
